//include

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3001;

var bodyParser = require('body-parser');
//app.use(bodyParser.json());
app.use(express.json());
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested_With, Content-Type, Accept");
  next();
});

var requestjson = require('request-json');

var path = require('path');

var urlUsuarios    = 'https://api.mlab.com/api/1/databases/glazo/collections/usuario?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';
var urlMovimientos = 'https://api.mlab.com/api/1/databases/glazo/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';

var clienteMlab = requestjson.createClient(urlMovimientos);

var usuarioMlab = requestjson.createClient(urlUsuarios);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

//app.get('/usuario', function(req,res){
//  var urlDB = urlUsuarios + "?q={'usuario':'" + this.usuario + "', 'clave':'" + this.clave + "'}";
//  usuarioMlab.get('urlDB', function(err, resM, body){
//    if(err) {
//      console.log(err);
//    }
//    else{
//      res.send(body);
//    }
//  });
//})
